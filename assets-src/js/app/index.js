// index.js

(function($) {
    var headerEl = $('.layout__header');
    var navEl = '#nav-menu--main';
    var wpadminbar = $('#wpadminbar');

    uiNav();
    frontpageSlide();

    function uiNav() {
        $('[data-target="' + navEl + '"]').on('click', function() {
            $('html').toggleClass('show-nav');
            $(this)
                .find('.hamburger')
                .toggleClass('is-active');
        });

        $('html').on('mouseup touchmove', function(e) {
            if ($('html').hasClass('show-nav')) {
                var target = $(navEl).is(e.target);
                var contain = headerEl.has(e.target).length;
                if (!target && !contain) {
                    $(navEl).removeClass('in');
                    $('html').removeClass('show-nav');
                    headerEl
                        .find('.navbar-toggle')
                        .attr('aria-expanded', 'false')
                        .find('.hamburger')
                        .removeClass('is-active');
                }
            }
        });
    }

    function frontpageSlide() {}
})(jQuery);
