// google-maps.js

(function($) {
    createMaps();

    function createMaps() {
        $('.map-item').each(function () {
            map = newMap($(this));
        });
    }

    function newMap($el) {
        var $markers = $el.find('.marker');
        // See document:
        // https://developers.google.com/maps/documentation/javascript/styling
        // https://mapstyle.withgoogle.com/
        var styles = [];
        var args = {
            zoom: 16,
            scrollwheel: false,
            disableDefaultUI: true,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: styles
        };
        var map = new google.maps.Map( $el[0], args);

        map.markers = [];
        $markers.each(function () {
            addMapMarker($(this), map);
        });

        setMapCenter(map);
        return map;
    }

    function addMapMarker($marker, map) {
        var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
        var markerConfig = {
            position: latlng,
            map: map
        };
        if ($marker.attr('data-icon')) {
            markerConfig.icon = $marker.attr('data-icon');
        }
        var marker = new google.maps.Marker(markerConfig);

        map.markers.push(marker);
        if ($marker.html()) {
            var infowindow = new google.maps.InfoWindow({
                content: $marker.html()
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
        }
    }

    function setMapCenter(map) {
        var bounds = new google.maps.LatLngBounds();
        $.each(map.markers, function (i, marker) {
            var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
            bounds.extend(latlng);
        });

        if (map.markers.length == 1) {
            map.setCenter(bounds.getCenter());
            map.setZoom(16);
        } else {
            map.fitBounds(bounds);
        }
    }
})(jQuery);
