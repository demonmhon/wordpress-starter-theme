// scroll.js

(function($) {
    scrollToEl();

    function scrollToEl() {
        $('[data-scroll-ui="true"]').on('click', function () {
            var srcEl    = $(this);
            var targetSl = (srcEl.attr('href')) ? srcEl.attr('href') : srcEl.data('target');
            if (targetSl) {
                var targetEl = $(targetSl);
                if (targetEl.length) {
                    var speed = (srcEl.data('scroll-speed')) ? parseInt(srcEl.data('scroll-speed'), 10) : 1000;
                    $('html, body').animate({
                        scrollTop: targetEl.offset().top
                    }, speed);
                }
            }
            return false;
        });
    }
})(jQuery);
