<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<?php wp_head(); ?>
</head>
<?php

$logo = get_stylesheet_directory_uri() . '/images/logo.png';

if (function_exists('get_field')) {
    if (get_field('header_logo', 'option')) {
        $logo = get_field('header_logo', 'option');
    }
}

?>
<body <?php body_class(); ?>>
<div class="layout__site-wrapper">
    <a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>
    <div id="top"></div>
    <!-- Site header -->
    <header id="layout__header" class="layout__header">
        <div class="layout__header-container">
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-menu--main" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <div class="hamburger hamburger--slider">
                                <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                </div>
                            </div>
                        </button>
                        <a class="navbar-brand brand" href="<?php bloginfo('url'); ?>">
                            <span class="logo"><img src="<?php echo $logo ?>" alt="<?php bloginfo('name'); ?>" class="img-responsive"></span>
                            <span class="text"><?php bloginfo('name'); ?></span>
                        </a>
                    </div>
                    <div id="nav-menu--main" class="nav-menu--main collapse navbar-collapse">
                        <div class="main-menu--top">
                            <?php get_template_part('template-parts/search-form'); ?>
                            <!-- Main menu / site navigation -->
                            <ul class="main-menu main-menu--header-nav nav navbar-nav" id="main-menu--header-nav">
                                <li class="">
                                    <a href="#">Home</a>
                                </li>
                                <li class="">
                                    <a href="#">About</a>
                                </li>
                                <li class="">
                                    <a href="#">Contact</a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li class="dropdown-header">Nav header</li>
                                        <li><a href="#">Action</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <?php
                                // $nav_menu =
                                //     array(
                                //         'theme_location' => 'theme_main_nav',
                                //         'container'      => false,
                                //         'menu_id'        => 'main-menu--header-nav',
                                //         'menu_class'     => 'main-menu--header-nav nav navbar-nav',
                                //         'depth'          => 3,
                                //         'walker'         => new wp_bootstrap_navwalker()
                                //     );
                                // wp_nav_menu($nav_menu);
                            ?>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!-- Page content -->
    <div id="content"></div>
    <div id="layout__content" class="layout__content">
        <div class="layout__content-container">
