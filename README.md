# WordPress Starter Theme

The Stater WordPress theme. 

## Required Plugins

The theme won't display properly without follow plugins:
- [Advanced Custom Fields Pro](https://www.advancedcustomfields.com/)

## Theme Development

Please following the [WordPress Developer Handbook](https://developer.wordpress.org/themes/basics/) for theme development. For the the theme's asset files (CSS, JavaScript), [Node.js 8.x+](https://nodejs.org/) is required.

### Prerequisites

- [Node.js 8.x+](https://nodejs.org/)

### Start the development

Install the required packages with npm:
```bash
npm install
```

Start the script to compile the assets and watch for the file changes:
```bash
npm run dev
```

## Folder and file structure

```
/
├── assets
├── assets-src
├── fonts
├── img
├── inc
├── template-parts
└── templates
    index.php
    style.css
```
