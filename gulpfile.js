const gulp = require('gulp');
const banner = require('gulp-banner');
const cleanCSS = require('gulp-clean-css');
const cssbeautify = require('gulp-cssbeautify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const scss = require('gulp-sass');
const uglify = require('gulp-uglify');
const pkg = require('./package.json');

const comment =
    '/*\n' +
    ' * <%= pkg.name %> <%= pkg.version %>\n' +
    ' * <%= pkg.description %>\n' +
    ' * <%= pkg.homepage %>\n' +
    ' * <%= pkg.author %>\n' +
    ' */ \n';

const assetsConfig = {
    app: 'app',
    srcPath: {
        js: './assets-src/js/',
        scss: './assets-src/scss/'
    },
    targetPath: {
        js: './assets/',
        scss: './assets/'
    }
};

gulp.task('scss--process', function() {
    return gulp
        .src(assetsConfig.srcPath.scss + assetsConfig.app + '/style.scss')
        .pipe(scss())
        .on('error', logError)
        .pipe(cssbeautify())
        .pipe(rename('style.css'))
        .pipe(gulp.dest(assetsConfig.targetPath.scss))
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(
            banner(comment, {
                pkg: pkg
            })
        )
        .pipe(gulp.dest(assetsConfig.targetPath.scss));
});

gulp.task('scss', ['scss--process']);

gulp.task('uglify--process', function() {
    return gulp
        .src([
            assetsConfig.srcPath.js + 'vendor/bootstrap/collapse.js',
            assetsConfig.srcPath.js + 'vendor/bootstrap/dropdown.js',
            assetsConfig.srcPath.js + assetsConfig.app + '/index.js',
            assetsConfig.srcPath.js + assetsConfig.app + '/scroll.js'
        ])
        .pipe(concat('script.js'))
        .pipe(gulp.dest(assetsConfig.targetPath.js))
        .pipe(uglify())
        .on('error', logError)
        .pipe(rename('script.min.js'))
        .pipe(
            banner(comment, {
                pkg: pkg
            })
        )
        .pipe(gulp.dest(assetsConfig.targetPath.js));
});

gulp.task('uglify', ['uglify--process']);

gulp.task('watch', function() {
    gulp.watch(assetsConfig.srcPath.scss + assetsConfig.app + '/**/*.scss', [
        'scss'
    ]);
    gulp.watch(assetsConfig.srcPath.js + assetsConfig.app + '/**/*.js', [
        'uglify'
    ]);
});

gulp.task('default', ['scss', 'uglify']);

function logError(err) {
    console.log(err.toString());
    this.emit('end');
}
